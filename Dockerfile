FROM python:3.9-alpine

RUN apk add gcc libxml2-dev libxslt-dev python3-dev musl-dev

RUN pip install pytz flask feedgen python-dateutil gunicorn

COPY . .

CMD gunicorn -w 4 -b "0.0.0.0:8345" lfmb:app