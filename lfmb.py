import dateutil.parser
import json
import os
import pytz
from datetime import date
from flask import Flask, request, render_template, redirect
from feedgen.feed import FeedGenerator
from werkzeug.utils import secure_filename

app = Flask(__name__)

EST = pytz.timezone('US/Eastern')

def create_rss(title, slug, description, category, explicit):
    with open(f'json/{slug}.json', 'r') as json_file:
        json_contents = json.loads(json_file.read())
    generator = FeedGenerator()
    generator.author({'name': 'Middle Basement Productions', 'email': 'middlebasement@shanemoore.me'})
    generator.title(title)
    generator.link(href=f'https://middlebasement.shanemoore.me/{slug}.rss', rel='self')
    generator.description(description)
    generator.language('en')
    generator.load_extension('podcast')
    generator.podcast.itunes_author('Middle Basement Productions')
    generator.podcast.itunes_owner(name='Shane Moore', email='middlebasement@shanemoore.me')
    generator.podcast.itunes_category({'cat': category})
    generator.podcast.itunes_explicit(explicit)
    generator.podcast.itunes_image(f'https://middlebasement.shanemoore.me/static/{slug}/images/icon.jpg')
    for episode in json_contents:
        episode_generator = generator.add_entry()
        if 'guid' in episode:
            episode_generator.id(episode['guid'])
        else:
            episode_generator.id(f'https://middlebasement.shanemoore.me/static/{slug}/audio/{episode["filename"]}')
        episode_generator.title(episode['title'])
        episode_generator.description(episode['description'])
        episode_generator.published(EST.localize(dateutil.parser.parse(episode['published'])))
        episode_generator.podcast.itunes_explicit(episode['explicit'])
        episode_generator.podcast.itunes_duration(episode['duration'])
        episode_generator.podcast.itunes_summary(episode['description'])
        episode_generator.podcast.itunes_subtitle(episode['description'])
        episode_generator.enclosure(f'https://middlebasement.shanemoore.me/static/{slug}/audio/{episode["filename"]}', str(os.path.getsize(f'static/{slug}/audio/{episode["filename"]}')), 'audio/mpeg')
    return generator.rss_str(pretty=True)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['mp3']
    
@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        show = request.form['show']
        slug = request.form['slug']
        description = request.form['description']
        duration = request.form['duration']
        name = request.form['name']
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)
        if file and allowed_file(file.filename):
            with open(f'json/{show}.json', 'r') as json_file:
                json_contents = json.loads(json_file.read())
            json_contents.append({
                'filename': f'{slug}.mp3',
                'title': name,
                'description': description,
                'explicit': 'yes',
                'duration': duration,
                'published': date.today().isoformat()
            })
            with open(f'json/{show}.json', 'w') as json_file:
                json_file.write(json.dumps(json_contents))
            filename = secure_filename(file.filename)
            file.save(f'static/{show}/audio/{slug}.mp3')
            return redirect(request.url)
    return render_template('upload.html')

@app.route('/dave-and-cat-hate-puzzles.rss')
def dave_and_cat_hate_puzzles_rss():
    return create_rss('Dave & Cat Hate Puzzles', 'dave-and-cat-hate-puzzles', 'Dave and Cat hate puzzles, but they try to solve them anyway. Logic, math, and word puzzles abound', 'Comedy', 'yes')


@app.route('/scientifically-speaking.rss')
def scientifically_speaking_rss():
    return create_rss('Scientifically Speaking', 'scientifically-speaking', 'Deep dives into... well, everything. Myths debunked, problems solved, and new mysteries discovered every episode. Hosted by David Frank, Shane Moore, Colleen Higgins, Reid Allen, and Phillip Diffley.', 'Science', 'yes')
    

if __name__ == '__main__':
    app.config['DEBUG'] = True
    app.run(port=8346)
